package it.giufino;

public class Main {
	public static void main(String[] args) {
		Animale leone = new Leone();
		Animale gatto = new Gatto();
		leone.emettiVerso();
		gatto.emettiVerso();
	}
}
